﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Huffman
{
    abstract class FreqNode
    {
        public long Frequency;
    }

    class FreqSymbol : FreqNode
    {
        public byte Symbol;
    }

    class FreqBranch : FreqNode
    {
        public FreqNode Zero;
        public FreqNode One;
    }

    class Huffman
    {
        const int alphabetLength = 256;

        public static void Code(Stream input, Stream output)
        {
            long[] frequencies = createFrequencyChart(input);
            input.Position = 0;
            long fileSize = frequencies.Sum();
            writeChartToStream(frequencies, output);
            FreqBranch parentNode = buildFreqTree(frequencies);
            string[] symbolCodes = getSymbolCodes(parentNode);
            Queue<char> outQueue = new Queue<char>();
            for (int j = 1; j <= fileSize; j++)
            {
                int symbol = input.ReadByte();
                foreach (char bit in symbolCodes[symbol])
                    outQueue.Enqueue(bit);
                if (j == fileSize)
                {
                    while (outQueue.Count % 8 != 0)
                        outQueue.Enqueue('0');
                }
                while (outQueue.Count >= 8)
                {
                    int data = 0;
                    for (int i = 0; i < 8; i++)
                    {
                        data <<= 1;
                        if (outQueue.Dequeue() == '1')
                            data |= 1;
                    }
                    output.WriteByte((byte)data);
                }
            }
        }

        public static void Decode(Stream input, Stream output)
        {
            long[] frequencies = readChartFromStream(input);
            long fileSize = frequencies.Sum();
            FreqBranch parentNode = buildFreqTree(frequencies);
            Queue<bool> inQueue = new Queue<bool>();
            for (int j = 0; j < fileSize; j++)
            {
                FreqNode node = parentNode;
                while (!(node is FreqSymbol))
                {
                    if (inQueue.Count == 0)
                    {
                        int word = input.ReadByte();
                        for (int i = 7; i >= 0; i--)
                        {
                            bool c = (word & (1 << i)) != 0;
                            inQueue.Enqueue(c);
                        }
                    }
                    FreqBranch branch = node as FreqBranch;
                    node = inQueue.Dequeue() ? branch.One : branch.Zero;
                }
                FreqSymbol symbol = node as FreqSymbol;
                output.WriteByte(symbol.Symbol);
            }
        }

        static long[] createFrequencyChart(Stream input)
        {
            long[] result = new long[alphabetLength];
            for (; ; )
            {
                int symbol = input.ReadByte();
                if (symbol < 0)
                    break;
                result[symbol]++;
            }
            return result;
        }

        static FreqBranch buildFreqTree(long[] frequencies)
        {
            SortedQueue<long, FreqNode> tree = new SortedQueue<long, FreqNode>();
            for (int i = 0; i < frequencies.Length; i++)
            {
                FreqSymbol symbol = new FreqSymbol
                {
                    Frequency = frequencies[i],
                    Symbol = (byte)i
                };
                if (frequencies[i] > 0)
                    tree.Enqueue(frequencies[i], symbol);
            }
            while (tree.Count > 1)
            {
                FreqNode one = tree.Dequeue();
                FreqNode zero = tree.Dequeue();
                FreqBranch parent = new FreqBranch
                {
                    One = one,
                    Zero = zero,
                    Frequency = one.Frequency + zero.Frequency,
                };
                tree.Enqueue(parent.Frequency, parent);
            }
            FreqBranch result = tree.Dequeue() as FreqBranch;
            return result;
        }

        static string[] getSymbolCodes(FreqBranch parentNode)
        {
            string[] result = new string[alphabetLength];
            Stack<Tuple<FreqNode, string>> nodes = new Stack<Tuple<FreqNode, string>>();
            nodes.Push(Tuple.Create(parentNode.One, "1"));
            nodes.Push(Tuple.Create(parentNode.Zero, "0"));
            while (nodes.Count > 0)
            {
                var pop = nodes.Pop();
                FreqNode node = pop.Item1;
                string code = pop.Item2;
                if (node is FreqBranch)
                {
                    FreqBranch branch = node as FreqBranch;
                    nodes.Push(Tuple.Create(branch.One, code + "1"));
                    nodes.Push(Tuple.Create(branch.Zero, code + "0"));
                }
                else
                {
                    FreqSymbol symbol = node as FreqSymbol;
                    result[symbol.Symbol] = code;
                }
            }
            return result;
        }

        static void writeToStream(long data, byte bitsPerSymb, Stream output)
        {
            for (int i = 0; i < bitsPerSymb; i += 8)
            {
                byte word = (byte)data;
                output.WriteByte(word);
                data >>= 8;
            }
        }

        static long readFromStream(byte bitsPerSymb, Stream input)
        {
            long data = 0;
            for (int i = 0; i < bitsPerSymb; i += 8)
            {
                long word = (byte)input.ReadByte();
                data |= word << i;
            }
            return data;
        }

        static void writeChartToStream(long[] frequencies, Stream output)
        {
            long fileSize = frequencies.Sum();
            long maxFreq = frequencies.Max();
            byte bitsPerFreq = 8;
            if (maxFreq >= 1 << 8)
                bitsPerFreq = 16;
            if (maxFreq >= 1 << 16)
                bitsPerFreq = 32;
            if (maxFreq >= 1L << 32)
                bitsPerFreq = 64;
            bool symInd = frequencies.Count(x => x > 0) * (bitsPerFreq + 8) < alphabetLength * bitsPerFreq;
            output.WriteByte((byte)(bitsPerFreq | (symInd ? 1 << 7 : 0)));
            if (symInd)
                output.WriteByte((byte)frequencies.Count(x => x > 0));
            for (int i = 0; i < frequencies.Length; i++)
            {
                if (symInd)
                {
                    if (frequencies[i] > 0)
                    {
                        output.WriteByte((byte)i);
                        writeToStream(frequencies[i], bitsPerFreq, output);
                    }
                }
                else
                    writeToStream(frequencies[i], bitsPerFreq, output);
            }
        }

        static long[] readChartFromStream(Stream input)
        {
            byte bitsPerFreq = (byte)input.ReadByte();
            bool symInd = (bitsPerFreq & (1 << 7)) != 0;
            bitsPerFreq = (byte)(bitsPerFreq & ~(1u << 7));
            long[] frequencies = new long[alphabetLength];
            if (symInd)
            {
                int numOfNonZero = input.ReadByte();
                for (int i = 0; i < numOfNonZero; i++)
                {
                    int j = input.ReadByte();
                    frequencies[j] = readFromStream(bitsPerFreq, input);
                }
            }
            else
            {
                for (int i = 0; i < frequencies.Length; i++)
                {
                    frequencies[i] = readFromStream(bitsPerFreq, input);
                }
            }
            return frequencies;
        }
    }
}
