﻿using System.Collections.Generic;
using System.Linq;

namespace Huffman
{
    class SortedQueue<TKey, TValue>
    {
        private SortedList<TKey, Queue<TValue>> sortedList = new SortedList<TKey, Queue<TValue>>();
        private long count = 0;
        public void Enqueue(TKey key, TValue item)
        {
            count++;
            if (!sortedList.ContainsKey(key))
                sortedList.Add(key, new Queue<TValue>());
            sortedList[key].Enqueue(item);
        }
        public TValue Dequeue()
        {
            count--;
            KeyValuePair<TKey, Queue<TValue>> pair = sortedList.First();
            TValue result = pair.Value.Dequeue();
            if (pair.Value.Count == 0)
                sortedList.Remove(pair.Key);
            return result;
        }
        public long Count
        {
            get { return count; }
        }
    }
}
