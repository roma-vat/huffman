﻿using System;
using System.IO;

namespace Huffman
{
    class Program
    {
        static void Main(string[] args)
        {
            foreach (string fileName in args)
            {
                try
                {
                    string huffmanExtention = ".huffman";
                    bool decode = fileName.EndsWith(huffmanExtention);
                    string outFileName = decode ? fileName.Remove(fileName.Length - huffmanExtention.Length)
                        :  fileName + huffmanExtention;
                    File.Delete(outFileName);
                    using (FileStream reader = File.OpenRead(fileName), writer = File.OpenWrite(outFileName))
                    {
                        if (decode)
                            Huffman.Decode(reader, writer);
                        else
                            Huffman.Code(reader, writer);
                    }
                }
                catch (Exception) { };
            }
        }
    }
}
